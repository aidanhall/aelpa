This was created by extracting my face customisations out of init.el.
It's mostly the same as the default, with the only notable change being
background colours for `display-line-numbers-mode'.