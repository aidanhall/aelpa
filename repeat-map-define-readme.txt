This package provides the `repeat-map-define' macro,
which makes it easier to define repeat maps using the built-in `repeat' package.
This provides a simpler alternative to packages like `hydra' and `transient',
which should work decently well for a single user.

E.g.
(repeat-map-define
 defun-repeat-map
 ("a" beginning-of-defun)
 ("e" end-of-defun))

The repeat map itself can be bound to a key to enable the repeating bindings
without performing one of the associated functions:

(global-set-key (kbd "C-x w") defun-repeat-map)

Please see the help for the function, as well as my blog post about it:
https://aidanhall.gitlab.io/website/blog/22-05-17_21.46.html