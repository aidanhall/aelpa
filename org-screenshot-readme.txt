
NOTE: This library requires external screenshot taking executable "scrot",
which is available as a package from all major Linux distribution. If your
distribution does not have it, source can be found at:

http://freecode.com/projects/scrot

org-screenshot.el have been tested with scrot version 0.8.

Usage:

  (require 'org-screenshot)

 Available commands with default bindings

 `org-screenshot-take'              C-c M-s M-t  and   C-c M-s M-s

       Take the screenshot, C-u argument delays 1 second, double C-u 2 seconds
       triple C-u 3 seconds, and subsequent C-u add 2 seconds to the delay.

       Screenshot area is selected with the mouse, or left-click on the window
       for an entire window.

 `org-screenshot-rotate-prev'       C-c M-s M-p   and C-c M-s C-p

       Rotate screenshot before the point to one before it (sorted by date)

 `org-screenshot-rotate-next'       C-c M-s M-n   and C-c M-s C-n

       Rotate screenshot before the point to one after it

 `org-screenshot-show-unused'       C-c M-s M-u   and C-c M-s u

       Open dired buffer with screenshots that are not used in current
       Org buffer marked

The screenshot take and rotate commands will update the inline images
if they are already shown, if you are inserting first screenshot in the Org
Buffer (and there are no other images shown), you need to manually display
inline images with C-c C-x C-v

Screenshot take and rotate commands offer user to continue by by using single
keys, in a manner similar to to "repeat-char" of keyboard macros, user can
continue rotating screenshots by pressing just the last key of the binding

For example: C-c M-s M-t creates the screenshot and then user can
repeatedly press M-p or M-n to rotate it back and forth with
previously taken ones.
