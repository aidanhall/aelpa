;;; sfeed.el --- An Emacs UI for viewing sfeed data. -*- lexical-binding: t; -*-

;; Copyright (C) 2024  Aidan Hall

;; Author: Aidan Hall <aidan.hall202@gmail.com>
;; Keywords: news, rss
;; Package-Requires: (vtable cl-lib shr)
;; URL: https://gitlab.com/aidanhall/sfeed.el
;; Version: 0.0.3

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; * Introduction
;;
;; This package provides a highly simplistic and streamlined interface
;; for reading RSS/Atom feed data that has been parsed by sfeed
;; (see <https://codemadness.org/sfeed-simple-feed-parser.html>).
;;
;; Rather than individually tracking whether each individual feed item
;; has been read, we only track the timestamp of the newest item in
;; each feed that has been read. This is sufficient to filter out old
;; items.
;;
;; * Usage
;;
;; To retrieve new feed items, run the sfeed_update command, or
;; execute the `sfeed-update' command, which calls it.
;;
;; By default, URLs in the item list and reading buffer are not made
;; clickable. If this behaviour is desired, use `goto-address-mode',
;; like so:
;;
;; (add-hook 'sfeed-mode-hook 'goto-address-mode)
;;
;; This package creates opens two hidden buffers with major mode
;; `sfeed-data-file-mode' for each feed (for the data & read files).
;; If you use a package like `desktop', that restores opened buffers,
;; you may wish to explicitly prevent these from being saved, for
;; example, by adding `sfeed-data-file-mode' to
;; `desktop-modes-not-to-save'.
;;
;; * Customization
;;
;; See the customization group `sfeed' for customization options.
;;
;; * Default Keybindings
;; ** All SFeed Buffers
;; - `u' :: Run `sfeed-update'.
;; - `g' :: Update and display the list of feed items.
;; - `R' :: Read a comma-separated list of feeds, and mark all items
;;          in each as unread.
;; - `D' :: Read a comma-separated list of feeds, and mark all items
;;          in each as read.
;;
;; ** Item List (`sfeed')
;; - `b' :: Open the selected item's URL with `browse-url'.
;; - `v', `RET' :: Read the current item's content in Emacs.
;; - `c' :: Set the current item as the most recently read in its feed.
;;          This also happens with `b' and `v'.
;; - `d' :: Mark all items in the current item's feed as read.
;; - `r' :: Mark all items in the current item's feed as unread.
;;
;; ** Reading Buffer
;; See `special-mode-map' and `shr-map'.
;; 
;; The point is initially placed on the title of the post, which is
;; set as a hyperlink with the item's URL, so you can press RET to
;; immediately open the URL.

;;; Code:

(require 'vtable)
(require 'cl-lib)
(require 'shr)

(defcustom sfeed-update-executable
  (executable-find "sfeed_update")
  "The sfeed_update executable."
  :type '(file :must-match t)
  :group 'sfeed)

(defcustom sfeed-path
  "~/.sfeed/feeds"
  "The sfeedpath, where sfeed data is stored."
  :type 'directory
  :group 'sfeed)

(defcustom sfeed-read-directory
  "~/.sfeed/read"
  "The directory storing the most recent read post timestamp for each feed."
  :type 'directory
  :group 'sfeed)

(defcustom sfeed-post-date-format
  "%x"
  "How to format post dates.
This is passed to `format-time-string'."
  :type 'string
  :group 'sfeed)

(defcustom sfeed-item-list-actions
  '("RET" sfeed-read-item
    "v" sfeed-read-item
    "c" sfeed-check
    "d" sfeed-mark-items-whole-feed-read
    "r" sfeed-mark-items-whole-feed-unread
    "b" sfeed-browse)
  "Actions to apply to the current item in the `sfeed' item list.
It takes the following form:
`(KEY ACTION KEY ACTION...)'.
Each ACTION is a function that gets called with the feed item at `point'
as an argument when KEY is pressed.
An item is an `sfeed-item' struct.
This is passed as the `:actions' parameter of `make-vtable'.
See Info node `(vtable)Making A Table' for more information."
  :type '(repeat (list :inline t key (function :tag "Action")))
  :group 'sfeed)

(cl-defstruct (sfeed-item (:constructor sfeed-item-create)
                          (:copier nil)
                          (:type list))
  "An sfeed item, with fields as defined in man page `sfeed(5)'.
It is represented as a list, so it can be passed to vtable verbatim."
  (feed "" :type string)
  (timestamp 0 :type integer)
  (title "" :type string)
  (link "" :type string)
  (content-start 0 :type integer)
  (content-end 0 :type integer)
  (content-type "plain" :type string)
  (id "" :type string)
  (author "" :type string))

(defun sfeed-feed-buffer (feed)
  "Get a buffer associated with FEED's file."
  (with-current-buffer
      (find-file-noselect (file-name-concat sfeed-path feed))
    (rename-buffer (concat " " feed) t)
    (sfeed-data-file-mode)
    (current-buffer)))

;;;###autoload
(defun sfeed-update ()
  "Run sfeed_update."
  (interactive)
  (make-process :name "sfeed_update"
                :buffer "*SFeed Update*"
                :command (list sfeed-update-executable)
                :sentinel
                (lambda (_ event)
                  (message "sfeed_update %s"
                           (string-trim-right event)))))

(defun sfeed-parse-new-items (oldest &optional buffer)
  "Parse items in BUFFER no older than OLDEST.
OLDEST is a UNIX timestamp.
Returns a list of `sfeed-item' objects."
  (cl-flet ((next-field ()
              (buffer-substring (point) (1- (search-forward "	")))))
    (with-current-buffer
        (or buffer (current-buffer))
      (auto-revert-mode 0)
      (revert-buffer t t t)
      (let ((feed (file-relative-name (buffer-file-name)
                                      sfeed-path)))
        (goto-char (point-min))
        (cl-loop while (not (eobp))
                 for timestamp = (string-to-number (next-field))
                 ;; This assumes items are ordered by timestamp,
                 ;; which they are by default.
                 while (< oldest timestamp)
                 collect
                 (list
                  feed
                  timestamp
                  (next-field)           ; title
                  (next-field)           ; link
                  (point)
                  (1- (search-forward "	")) ; content
                  (next-field)              ; content-type
                  (next-field)          ; id
                  (next-field))         ; author
                 do (forward-line))))))

(define-derived-mode sfeed-mode special-mode "SFeed"
  "Base major mode for SFeed buffers."
  :group 'sfeed
  (toggle-truncate-lines 1))
(define-key sfeed-mode-map "u" #'sfeed-update)
(define-key sfeed-mode-map "g" #'sfeed)
(define-key sfeed-mode-map "R" #'sfeed-reset-feeds)
(define-key sfeed-mode-map "D" #'sfeed-catchup-feeds)

(define-derived-mode sfeed-data-file-mode fundamental-mode "SFeed Data"
  "Major mode for buffers associated with SFeed data files.")

(defun sfeed-get-last-check-file (feed)
  "Produce a buffer for FEED's last-check file.
This may create the buffer, and `sfeed-read-directory',
which contains its associated file."
  (make-directory sfeed-read-directory t)
  (with-current-buffer
      (find-file-noselect (file-name-concat sfeed-read-directory feed))
    ;; Make the buffer invisible
    (rename-buffer (concat " " feed) t)
    (sfeed-data-file-mode)
    (current-buffer)))

(defun sfeed-get-latest-checked (feed)
  "Get the latest timestamp of read items in FEED.
Returned as a UNIX timestamp."
  (with-current-buffer (sfeed-get-last-check-file feed)
    ;; `string-to-number' returns 0 on failure; a good default.
    (string-to-number (buffer-string))))

(defun sfeed-set-latest-checked (feed timestamp)
  "Set the latest read time of FEED to TIMESTAMP."
  (with-current-buffer (sfeed-get-last-check-file feed)
    ;; `string-to-number' returns 0 on failure; a good default.
    (erase-buffer)
    (insert (number-to-string timestamp))
    (save-buffer)))


(defun sfeed-check (item &optional time)
  "Get ITEM's feed's last check time, and update to TIME if newer.
ITEM's timestamp is used if TIME is omitted."
  (let* ((feed (sfeed-item-feed item))
         (previous-check (sfeed-get-latest-checked feed))
         (timestamp (or time
                        (sfeed-item-timestamp item))))
    (when (< previous-check timestamp)
      (sfeed-set-latest-checked feed timestamp))
    previous-check))

(defun sfeed-reset-last-read (feed timestamp)
  "Reset the last read time for FEED to TIMESTAMP."
  (sfeed-set-latest-checked feed timestamp)
  (message "Reset last read time of %s to: %s"
           feed
           (format-time-string sfeed-post-date-format timestamp)))

(defun sfeed-mark-whole-feed-read (feed)
  "Set FEED's last read time to its newest timestamp.
This is assumed to be the timestamp on the first line."
  (with-current-buffer (sfeed-feed-buffer feed)
    ;; Skip empty feeds
    (when (< 0 (buffer-size))
      (goto-char (point-min))
      (sfeed-reset-last-read
       feed
       (read (current-buffer))))))

(defun sfeed-read-feed-list (prompt)
  "Read a list of sfeed feeds, displaying PROMPT."
  (let ((all-feeds (sfeed-get-feeds)))
    (or (completing-read-multiple
         prompt
         all-feeds
         nil
         nil)
        all-feeds)))

(defun sfeed-reset-feeds (feeds)
  "Remove the last read indicator for all supplied FEEDS."
  (interactive
   (list (sfeed-read-feed-list "Feeds (comma-separated) to reset (all by default): ")))
  (dolist (feed feeds)
    (sfeed-reset-last-read feed 0)))

(defun sfeed-catchup-feeds (feeds)
  "Mark all supplied FEEDS as read."
  (interactive
   (list (sfeed-read-feed-list "Feeds (comma-separated) to mark as read (all by default): ")))
  (dolist (feed feeds)
    (sfeed-mark-whole-feed-read feed)))

(defun sfeed-mark-items-whole-feed-unread (item)
  "Reset the last read time for ITEM's feed to 0."
  (sfeed-reset-last-read (car item) 0))

(defun sfeed-mark-items-whole-feed-read (item)
  "Mark ITEM's whole feed as read."
  (sfeed-mark-whole-feed-read (car item)))

(defun sfeed-browse (item)
  "Browse ITEM's URL."
  (sfeed-check item)
  (browse-url (sfeed-item-link item)))

(defun sfeed-read-item (item)
  "Display the supplied ITEM."
  (if (= (sfeed-item-content-start item)
         (sfeed-item-content-end item))
      (message "Item has no content")
    (sfeed-check item)
    (pop-to-buffer "*SFeed Read*")
    (read-only-mode 0)
    (erase-buffer)
    (insert-buffer-substring
     (sfeed-feed-buffer
      (sfeed-item-feed item))
     (sfeed-item-content-start item)
     (sfeed-item-content-end item))
    (goto-char (point-min))
    ;; Handle escapes generated by sfeed.
    (while (re-search-forward "\\\\[\\\\nt]" nil t)
      (replace-match
       (pcase (char-before)
         ('?\\ "\\\\")
         ('?n "
")
         ('?t "	"))))
    ;; Render the content
    (goto-char (point-min))
    ;; Add a header containing the title, author and creation time.
    (insert "<h1><a href=\"" (sfeed-item-link item) "\">"
            (sfeed-item-title item)
            "</a></h1><p><em>"
            ;; Use the author field if available, default to feed name.
            (let ((author (sfeed-item-author item)))
              (if (string-empty-p author)
                  (sfeed-item-feed item)
                author))
            "</em> — "
            (format-time-string sfeed-post-date-format
                                (sfeed-item-timestamp item))
            "</em></p>
")
    ;; We assume feeds content is HTML if not plain,
    ;; and that plain content shouldn't be processed.
    (shr-render-region (point-min)
                       (if (string= "html"
                                    (sfeed-item-content-type item))
                           (point-max)
                         (point)))
    (goto-char (point-min))
    (sfeed-mode)))

(defun sfeed-get-feeds ()
  "Produce a list of all sfeed feeds."
  (directory-files sfeed-path nil directory-files-no-dot-files-regexp))

(defun sfeed-get-new-items (feeds)
  "Get all new items in FEEDS.
Each feed the name of an sfeed feed."
  (mapcan
   (lambda (feed)
     (sfeed-parse-new-items
      (sfeed-get-latest-checked feed)
      (sfeed-feed-buffer feed)))
   feeds))

;;;###autoload
(defun sfeed ()
  "Display new feed items in a table.
Subsequent calls will only display items more recent than the last
call."
  (interactive)
  (switch-to-buffer "*SFeed*")
  (sfeed-mode)
  (read-only-mode 0)
  (erase-buffer)
  ;; TODO: Make the initial column widths customizable.
  (make-vtable
   :columns (eval-when-compile
              `((:name "Feed"
                       :min-width 10
                       :width "10%")
                ,(make-vtable-column
                  :name "Posted"
                  :primary 'ascend
                  :formatter (lambda (timestamp)
                               (propertize
                                (format-time-string
                                 sfeed-post-date-format
                                 timestamp)
                                'face 'vtable))
                  ;; Approximate width of DD/MM/YYYY in characters
                  :width 12)
                (:name "Title"
                       :min-width 35
                       :width "50%")
                (:name "Link"
                       :min-width 30
                       :width "20%")))
   :objects-function
   (lambda ()
     (message "Getting new feed items...")
     (sfeed-get-new-items
      (sfeed-get-feeds)))
   :actions sfeed-item-list-actions)
  (read-only-mode 1))

(provide 'sfeed)
;;; sfeed.el ends here
