;;; mom-mode.el --- Support for Groff Mom -*- lexical-binding: t; -*-

;; Copyright (C) 2022 Hall


;; Version: 0.0.2
;; Author: Aidan Hall <aidan.hall202@gmail.com>
;; Created: 7 Nov 2022

;; Keywords: groff mom
;; URL: https://example.com/
;;; Code:

(require 'nroff-mode)

(defvar mom-heading-string ".HEADING ")
(defvar mom-imenu-expression
  '((nil (concat "^\\\"" mom-heading-string "\"?\\([^\"\n]*\\)\"?$") 1)))

(defun mom-outline-level ()
  "The outline level for mom.  Based on `nroff-outline-level'."
  (save-excursion
    (looking-at outline-regexp)
    (skip-chars-forward mom-heading-string)
    (string-to-number (buffer-substring (point) (+ 1 (point))))))

(defvar mom-list-item ".ITEM")
(defun mom-insert-item ()
  "Insert one `mom-list-item'."
  (interactive "*")
  (insert "\n" mom-list-item "\n"))

(defvar mom-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map (kbd "M-<return>") #'mom-insert-item)
    map)
  "Major mode keymap for `mom-mode'.")

(define-derived-mode mom-mode nroff-mode "Groff Mom"
  "A major mode for Groff documents using the mom macros."
  (setq-local outline-regexp "\\.HEADING[ ]+[1-7]+"
              outline-level 'mom-outline-level))

(provide 'mom-mode)
;;; mom-mode.el ends here
