* Introduction

This package provides a highly simplistic and streamlined interface
for reading RSS/Atom feed data that has been parsed by sfeed
(see <https://codemadness.org/sfeed-simple-feed-parser.html>).

Rather than individually tracking whether each individual feed item
has been read, we only track the timestamp of the newest item in
each feed that has been read. This is sufficient to filter out old
items.

* Usage

To retrieve new feed items, run the sfeed_update command, or
execute the `sfeed-update' command, which calls it.

By default, URLs in the item list and reading buffer are not made
clickable. If this behaviour is desired, use `goto-address-mode',
like so:

(add-hook 'sfeed-mode-hook 'goto-address-mode)

This package creates opens two hidden buffers with major mode
`sfeed-data-file-mode' for each feed (for the data & read files).
If you use a package like `desktop', that restores opened buffers,
you may wish to explicitly prevent these from being saved, for
example, by adding `sfeed-data-file-mode' to
`desktop-modes-not-to-save'.

* Customization

See the customization group `sfeed' for customization options.

* Default Keybindings
** All SFeed Buffers
- `u' :: Run `sfeed-update'.
- `g' :: Update and display the list of feed items.
- `R' :: Read a comma-separated list of feeds, and mark all items
         in each as unread.
- `D' :: Read a comma-separated list of feeds, and mark all items
         in each as read.

** Item List (`sfeed')
- `b' :: Open the selected item's URL with `browse-url'.
- `v', `RET' :: Read the current item's content in Emacs.
- `c' :: Set the current item as the most recently read in its feed.
         This also happens with `b' and `v'.
- `d' :: Mark all items in the current item's feed as read.
- `r' :: Mark all items in the current item's feed as unread.

** Reading Buffer
See `special-mode-map' and `shr-map'.

The point is initially placed on the title of the post, which is
set as a hyperlink with the item's URL, so you can press RET to
immediately open the URL.