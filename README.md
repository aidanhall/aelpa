# Aidan's Emacs Lisp Package Archive

This is an unnecessarily complicated way for me to manage and distribute my Emacs packages.

To use AElpa, add it to your package archives.  I cannot seriously
recommend this, and I don't really know what you're doing if you found
this anyway.

``` emacs-lisp
(add-to-list 'package-archives '("aelpa" . "https://aidanhall.gitlab.io/aelpa"))
```

If you can read Lisp code, you can view the
<a href="archive-contents">archive contents file</a>
to get a sense of what's in here.
